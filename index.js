const express = require('express');
const app = express();
const { port } = require('./config.js');
const apiRouter = require('./routes/api');

// routes
app.use('/', apiRouter);

// server
app.listen(port, () => {
  console.log('Serwer słucha... http://localhost:' + port);
})